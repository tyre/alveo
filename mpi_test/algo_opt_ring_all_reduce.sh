#!/bin/bash
ele=(4000)

for i in "${ele[@]}"  
do
    for n in 4
    do
        /mnt/scratch/dkoutsou/openmpi/bin/mpirun -np $n --hostfile hostfile ./algo_opt_ring_all_reduce $i 1
    done
done