#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <assert.h>

int main(int argc, char** argv) {
  if (argc != 3) {
    fprintf(stderr, "Usage:  data_size_KB num_trials\n");
    exit(1);
  }

  int bytes = 1024;
  int num_elements = bytes/sizeof(int);
  if (argc >= 2)
    {
        bytes = strtol(argv[1], NULL, 10);
        num_elements = bytes/sizeof(int); 
    }
  
  int num_trials = 1;
  if (argc >= 3)
  {
      num_trials = strtol(argv[2], NULL, 10);
  } 

  printf("bytes:%d, num_elements:%d, num_trials:%d\n", bytes, num_elements, num_trials);

  MPI_Init(NULL, NULL);

  int world_rank, size;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  double total_mpi_time = 0.0;
  int i;
  int fnderr=0;;

    int *in, *out, *sol;

    in = (int *)malloc( num_elements * sizeof(int) );
    out = (int *)malloc( num_elements * sizeof(int) );
    sol = (int *)malloc( num_elements * sizeof(int) );
  
  double starttime, endtime, duration;

  ////////////////////////////////
  //MPI All Reduce
  ////////////////////////////////
    // printf("//////////////MPI All Reduce//////////\n");
  
    for (i=0; i<num_elements; i++)
    {
        *(in + i) = i+1;
        *(sol + i) = (i+1)*size;
        *(out + i) = 0;
    }

  //first round to warm up, not included in time counting
  for (i = 0; i < num_trials+1; i++) {
    // Time All-reduce
    MPI_Barrier(MPI_COMM_WORLD);
    starttime = MPI_Wtime();
    MPI_Allreduce( in, out, num_elements, MPI_INT, MPI_SUM, MPI_COMM_WORLD );
    MPI_Barrier(MPI_COMM_WORLD);
    endtime = MPI_Wtime();
    duration = endtime - starttime;

    if(i>=1)
    {
      total_mpi_time = total_mpi_time + duration;
    }
    
    if(world_rank==0)
    {
	    printf("iteration:%d, time[s]:%f\n",i, duration);
    }
  }

  // Print off timing information
  if (world_rank == 0) {
    printf("Data size = %d, Trials = %d\n", num_elements * (int)sizeof(int),
           num_trials);
    printf("Avg MPI_all_reduce time [us]= *%lf\n", total_mpi_time*1000000.0 / num_trials);
  }

  MPI_Barrier(MPI_COMM_WORLD);
  //Compare results
  int error_cnt = 0;
  for (i = 0; i < num_elements; i++)
  {
      if(out[i] != sol[i])
      {
          printf("Rank %d, Error: element %d expected :%d, result:%d\n",world_rank, i, sol[i], out[i] );
          error_cnt ++;
      }
  }
  if (error_cnt == 0)
    printf("Rank %d results correct\n", world_rank);

  
  /////////////////////////////////
  // Ring All Reduce
  ////////////////////////////////
    MPI_Barrier(MPI_COMM_WORLD);
    for (i=0; i<num_elements; i++)
    {
        *(in + i) = i+1;
        *(sol + i) = (i+1)*size;
        *(out + i) = 0;
    }
    
    printf("//////////////Ring All Reduce//////////\n");
    unsigned int next_in_ring = (world_rank + 1) % size;
    unsigned int prev_in_ring = (world_rank + size -1 ) % size;

    int num_elements_div_size = num_elements / size;
    // send current chunk
    int curr_send_chunk = world_rank;
    // receive previous chunk 
    int curr_recv_chunk = (curr_send_chunk + size -1) % size;

    int * rx_buf = (int *)malloc( num_elements_div_size * sizeof(int) );

    MPI_Barrier(MPI_COMM_WORLD);
    // Share reduce stage
    for (i=0; i<size-1; i++)
    {   
        if(world_rank != 0)
        {
            //recv from previous rank
            MPI_Recv( rx_buf, num_elements_div_size, MPI_INT, prev_in_ring, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            //reduce with input buffer and put results back to input buffer
            for (int j=0; j< num_elements_div_size; j++)
            {
                in[num_elements_div_size*curr_recv_chunk + j] += rx_buf[j]; 
                // if(i == size -2)
                    // printf("Rank %d in[%d]:%d\n",world_rank,num_elements_div_size*curr_recv_chunk + j,in[num_elements_div_size*curr_recv_chunk + j]);
            }
        }
        MPI_Send( in + (curr_send_chunk*num_elements_div_size), num_elements_div_size, MPI_INT, next_in_ring, 0, MPI_COMM_WORLD );

        if(world_rank == 0)
        {
            //recv from previous rank
            MPI_Recv( rx_buf, num_elements_div_size, MPI_INT, prev_in_ring, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            //reduce with input buffer and put results back to input buffer
            for (int j=0; j< num_elements_div_size; j++)
            {
                in[num_elements_div_size*curr_recv_chunk + j] += rx_buf[j]; 
                // if(i == size -2)
                    // printf("Rank %d in[%d]:%d\n",world_rank,num_elements_div_size*curr_recv_chunk + j,in[num_elements_div_size*curr_recv_chunk + j]);
            }
        }

        //If last iteration, move the reduced result from input buffer to output buffer
        if (i == size -2)
            for(int j=0; j< num_elements_div_size; j++)
                out[curr_recv_chunk*num_elements_div_size+j] = in[num_elements_div_size*curr_recv_chunk + j];

        curr_send_chunk = curr_recv_chunk;
        curr_recv_chunk = (curr_recv_chunk + size - 1) % size;
        
    }

    // printf("rank:%d ",world_rank);
    // for(i=0;i<num_elements;i++)
    //     printf("in[%d]:%d ",i,in[i]);
    // printf("\n");

    //Share results stage
    for (i=0; i<size-1; i++)
    {       
        if(world_rank != 0)
        {
            //recv from previous rank
            MPI_Recv( out + curr_recv_chunk*num_elements_div_size, num_elements_div_size, MPI_INT, prev_in_ring, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }
        MPI_Send( out + (curr_send_chunk*num_elements_div_size), num_elements_div_size, MPI_INT, next_in_ring, 0, MPI_COMM_WORLD );

        if(world_rank == 0)
        {
            //recv from previous rank
            MPI_Recv( out + curr_recv_chunk*num_elements_div_size, num_elements_div_size, MPI_INT, prev_in_ring, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }

        curr_send_chunk = curr_recv_chunk;
        curr_recv_chunk = (curr_recv_chunk + size - 1) % size;

        // printf("iteration:%d, rank:%d ",i, world_rank);
        // for(int k=0;k<num_elements;k++)
        //     printf("out[%d]:%d ",k,out[k]);
        // printf("\n");
    }

    

    //Compare results
    error_cnt = 0;
    for (i = 0; i < num_elements; i++)
    {
        if(out[i] != sol[i])
        {
            printf("Rank %d, Error: element %d expected :%d, result:%d\n",world_rank, i, sol[i], out[i] );
            error_cnt ++;
        }
    }
    if (error_cnt == 0)
        printf("Rank %d results correct\n", world_rank);


    free( in );
    free( out );
    free( sol );
    MPI_Finalize();
}
